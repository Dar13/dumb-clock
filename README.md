## Dumb Clock

This is a dumb clock application, useful for when you just need a digital clock
visible on a multimonitor setup where no clocks are easily visible.

It allows scaling the text of the clock up and down, and retrieves the time via
your system clock.

There is no customization beyond the scale of the font.

### Rationale for existence

This application was written in anger after realizing that Gnome Shell does not
have an idiomatic way to have a clock on a non-primary screen. Since games are
typically run on the primary screen and are usually fullscreen, this is problematic.

## Contributing

You may request/provide changes to the application, but I will most likely refuse
to do them or reject changes unless they are code quality or application prettiness
suggestions/enhancements.

This is not because I don't appreciate them, but because I literally only need
a dumb digital clock and the only reason this application exists at all is because
GNOME Clocks seemingly cannot be configured to do this.
