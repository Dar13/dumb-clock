#include <cmath>
#include <cstdio>

#include <chrono>

#include <gtk/gtk.h>

#define UPDATE_INTERVAL 1

const char *clock_format_str = "<span font=\"%u\">%02u:%02u %s\n%04u-%02u-%02u</span>";

static guint font_scale = 18;
static gboolean font_scalers_enabled = FALSE;
static GtkWidget *font_scalers_container = NULL;

#define SCALE_INC(x)    (ceil(sqrt((x))))

void increase_font_scale(GtkWidget* widget, gpointer user_data)
{
    font_scale += SCALE_INC(font_scale);
}

void decrease_font_scale(GtkWidget* widget, gpointer user_data)
{
    font_scale -= SCALE_INC(font_scale);
}

GtkWidget *build_font_scalers()
{
    GtkWidget *font_scale_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    GtkWidget *font_scale_up = gtk_button_new_from_icon_name("list-add-symbolic");
    GtkWidget *font_scale_down = gtk_button_new_from_icon_name("list-remove-symbolic");

    gtk_widget_set_halign(font_scale_box, GTK_ALIGN_CENTER);
    gtk_widget_set_valign(font_scale_box, GTK_ALIGN_START);
    gtk_box_append(GTK_BOX(font_scale_box), font_scale_down);
    gtk_box_append(GTK_BOX(font_scale_box), font_scale_up);

    g_signal_connect(font_scale_up, "clicked", G_CALLBACK(increase_font_scale), NULL);
    g_signal_connect(font_scale_down, "clicked", G_CALLBACK(decrease_font_scale), NULL);
    
    return font_scale_box;
}

// Must call g_free on returned string
gchar* get_curr_time_string()
{
    // All of this is because C++20 support isn't complete in GCC/libstdc++ yet.
    auto curr_time_point = std::chrono::system_clock::now();
    std::time_t curr_time = std::chrono::system_clock::to_time_t(curr_time_point);
    auto calendar_time = std::localtime(&curr_time);

    int adjusted_hour = calendar_time->tm_hour;
    const char *am_pm = "AM";
    if (adjusted_hour >= 12)
    {
        am_pm = "PM";
        if (adjusted_hour > 12)
        {
            adjusted_hour -= 12;
        }
    }

    gchar *str = g_markup_printf_escaped(clock_format_str,
                                         font_scale,
                                         adjusted_hour,
                                         calendar_time->tm_min,
                                         am_pm,
                                         1900 + calendar_time->tm_year,
                                         1 + calendar_time->tm_mon,
                                         calendar_time->tm_mday);

    return str;
}

gboolean do_update_time(gpointer user_data)
{
    gchar *new_time_str = get_curr_time_string();

    gtk_label_set_markup(GTK_LABEL(user_data), new_time_str);

    g_free(new_time_str);

    gtk_widget_queue_draw(GTK_WIDGET(user_data));

    return TRUE;
}

static void enable_font_scalers(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *overlay = GTK_WIDGET(user_data);

    font_scalers_container = build_font_scalers();
    gtk_overlay_add_overlay(GTK_OVERLAY(overlay), font_scalers_container);
}

static void disable_font_scalers(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *overlay = GTK_WIDGET(user_data);

    // This deletes the overlay element (in this case, the font scaler buttons)
    gtk_overlay_remove_overlay(GTK_OVERLAY(overlay), font_scalers_container);
}

static void on_mouse_enter(GtkEventControllerMotion *controller, double x, double y, gpointer user_data)
{
    GtkWidget *window = GTK_WIDGET(user_data);
    if (font_scalers_enabled == FALSE)
    {
        font_scalers_enabled = TRUE;
        g_signal_emit_by_name(window, "enable-scale-buttons");
    }
}

static void on_mouse_leave(GtkEventControllerMotion *controller, double x, double y, gpointer user_data)
{
    GtkWidget *window = GTK_WIDGET(user_data);
    if (font_scalers_enabled == TRUE)
    {
        font_scalers_enabled = FALSE;
        g_signal_emit_by_name(window, "disable-scale-buttons");
    }
}

static void activate(GtkApplication *app, gpointer user_data)
{
    /* Prefer dark themes */
    GtkSettings *settings = gtk_settings_get_default();
    g_object_set(G_OBJECT(settings), "gtk-application-prefer-dark-theme", TRUE);

    /* Set the title appropriately */
    GtkWidget *window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Dumb Clock");

    /* Apply the theme to the application */
    GtkStyleContext *style_ctx = gtk_widget_get_style_context(GTK_WIDGET(window));
    gtk_style_context_add_provider(style_ctx, GTK_STYLE_PROVIDER(settings), GTK_STYLE_PROVIDER_PRIORITY_USER);

    GtkEventController *mouse_controller = gtk_event_controller_motion_new();

    GtkWidget *box = gtk_center_box_new();
    GtkWidget *label = gtk_label_new(NULL);

    gchar *label_text = get_curr_time_string();
    gtk_label_set_markup(GTK_LABEL(label), label_text);
    g_free(label_text);

    gtk_center_box_set_center_widget(GTK_CENTER_BOX(box), label);

    GtkWidget *overlay = gtk_overlay_new();

    gtk_overlay_set_child(GTK_OVERLAY(overlay), box);

    gtk_window_set_child(GTK_WINDOW(window), overlay);

    /* Hook up all the event handlers */
    /* 'enter' and 'leave' work well with the overlay widget, but not the window. *shrug* */
    g_signal_connect(mouse_controller, "enter", G_CALLBACK(on_mouse_enter), window);
    g_signal_connect(mouse_controller, "leave", G_CALLBACK(on_mouse_leave), window);
    gtk_widget_add_controller(overlay, mouse_controller);

    g_signal_new("enable-scale-buttons", G_TYPE_OBJECT, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
    g_signal_connect(window, "enable-scale-buttons", G_CALLBACK(enable_font_scalers), overlay);

    g_signal_new("disable-scale-buttons", G_TYPE_OBJECT, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
    g_signal_connect(window, "disable-scale-buttons", G_CALLBACK(disable_font_scalers), overlay);

    /* Show the window, now that it's all setup */
    gtk_widget_show(window);

    g_timeout_add_seconds(UPDATE_INTERVAL, do_update_time, label);
}

int main(int argc, char **argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("org.nmoore.dumb-clock", G_APPLICATION_FLAGS_NONE);

    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);

    return status;
}
